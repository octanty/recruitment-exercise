<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%loan}}".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property float $interest
 * @property int $duration
 * @property string $start_date
 * @property string $end_date
 * @property int $campaign
 * @property bool|null $status
 */
class LoanPage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%loan}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'interest', 'duration', 'start_date', 'end_date', 'campaign'], 'required'],
            [['user_id', 'duration', 'campaign'], 'default', 'value' => null],
            [['user_id', 'duration', 'campaign'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['start_date', 'end_date'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'interest' => 'Interest',
            'duration' => 'Duration',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'campaign' => 'Campaign',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\LoanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LoanQuery(get_called_class());
    }

    public static function getLoanByUserID($userID){
        $rows = (new \yii\db\Query())
            ->select(['id','user_id','amount','interest'])
            ->from('loan')
            ->where(['user_id' => $userID])
            ->limit(10)
            ->all();
        
        return $rows;
    }

}
