<?php

namespace app\controllers;

use Yii;
use app\models\UserPage;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use DateTime;

/**
 * UserController implements the CRUD actions for UserPage model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UserPage::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserPage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
     /*   return $this->render('view', [
            'model' => $this->findModel($id),
        ]);*/
           
        $model = $this->findModel($id);
        $age = $this->personAge($model->personal_code);
        return $this->render('view',array('model' => $model, 'age' => $age,
                            ));
    }

    /**
     * Creates a new UserPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserPage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        $model = new UserPage();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $age = $this->personAge($model->personal_code);
            return $this->redirect(['view', 
            array('id' => $model->id, 'age' => $age,
                                )]);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }



    
    public function personAge($personalIDCode, Datetime $date = null)
    {
        $firstNo = substr($personalIDCode, 0, 1);
        $total = 1700 + ceil($firstNo / 2) * 100;
        $year = $total + substr($personalIDCode, 1, 2);		         
        $month = substr($personalIDCode, 3, 2);
        $day = substr($personalIDCode, 5, 2);
        $birthDate = new Datetime($year.'-'.$month.'-'.$day);
        $date = new Datetime();
        return $date->diff($birthDate)->y;    
    }

    /**
     * Updates an existing UserPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
