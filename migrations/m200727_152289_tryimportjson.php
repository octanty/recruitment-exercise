<?php

use yii\db\Migration;
use yii\db\IlluminateDatabaseSeeder;
use yii\db\IlluminateSupportFacadesDB;

/**
 * Class m200726_150948_importjson
 */
class m200727_152289_tryimportjson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $json = file_get_contents(__DIR__ . '/users.json');
       
        $users = json_decode($json);
    
 
        foreach ($users as $user){
            DB::table('user')->insert([
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'personal_code' => $user->personal_code,
                'phone' => $user->phone,
                'active' =>  $user->active,
                'dead' =>  $user->dead,
                'lang' =>  $user->lang,
            ]);
        } 

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200726_150948_importjson cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200726_150948_importjson cannot be reverted.\n";

        return false;
    }
    */
}
