<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LoanPage */

$this->title = 'Create Loan Page';
$this->params['breadcrumbs'][] = ['label' => 'Loan Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
