<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserPage */

$this->title = 'Create User Page';
$this->params['breadcrumbs'][] = ['label' => 'User Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
